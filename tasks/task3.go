package main

import (
	"fmt"
)

func main() {
	var r float32 = 10.04

	fmt.Scanf("%f", &r)
	fmt.Println("R =", r)

	fmt.Printf("Area: %0.2f\n", area(r))
}

func area(r float32) (area float32) {
	var PI float32 = 3.14159265359
	q := 4 * r * r    // to find area of a square
	area = q - PI*r*r // it will substract area of circle from square, so remaining part is answer

	return area
}
