package main

import "fmt"

func main() {
	var a, b int = 3, 4

	fmt.Scanf("%d", &a)
	fmt.Scanf("%d", &b)
	fmt.Printf("a = %d, b = %d\n", a, b)

	a = a * b // assign value of a*b to a
	b = a / b // (a*b)/b = a, this assigns a to b
	a = a / b // (a*b)/b = b, because we assigned b = a in last line

	fmt.Printf("a = %d, b = %d\n", a, b)
}
